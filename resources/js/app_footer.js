(() => {
    let dtToday = new Date();
    let minDate = dtToday.toISOString().substr(0, 10);
    let elem = document.getElementById("return_date_at");

    if ( elem !== null ) {
        elem.min = minDate;
    }
})()