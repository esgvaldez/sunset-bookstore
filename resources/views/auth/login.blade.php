@extends('layouts.main')

@section('content')
    <div class="signin-form-container">

        <h1 class="h3 mb-3 fw-normal">Please sign in</h1>
        @if (session('fail_authentication'))
            <p class="text-danger">{{ session('fail_authentication') }}</p>
        @endif

        <form action="{{ route('login') }}" method="post">
            @csrf

            <div class="form-floating">
                <input type="email" class="form-control" name="email" id="email" placeholder="name@example.com">
                <label for="email">Email address</label>

                @error('email')
                    <p class="text-danger">{{ $message }}</p>
                @enderror

            </div>
            <div class="form-floating">
                <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                <label for="password">Password</label>

                @error('password')
                    <p class="text-danger">{{ $message }}</p>
                @enderror
            </div>

            <div class="checkbox mb-3">
                <label>
                <input type="checkbox" name="remember" id="remember"> Remember me
                </label>
            </div>
            <button class="w-100 btn btn-lg btn-primary" type="submit">Sign in</button>
        </form>
    </div>
    
@endsection