@extends('layouts.main')

@section('content')
    <div class="py-5">
        <div class="mb-4">
            <h1 class="m-0">Lend Book</h1> 
        </div>

        <div id="filters">
            <div class="mb-3">
                <p class="m-0">Book Title: <strong>{{ $book->title }}</strong></p>
                <p>Author: <strong>{{ $book->author }}</strong></p>

                <p>Library: <strong>{{ $book->library->name }}</strong></p>
            </div>
            <form method="post" action="{{ route('books.out') }}">
                @csrf
                @method('PUT')
                <input type="hidden" name="book_id" value="{{ $book->id }}">
                <input type="hidden" name="library_id" value="{{ $book->library->id }}">
                <div class="mb-3">
                    <label for="return_date_at" class="form-label">Date &amp; Time</label>
                    <input type="date" class="form-control" id="return_date_at" name="return_date_at">
                    <input type="time" class="form-control" id="return_time_at" name="return_time_at">
                    @error('return_date_at')
                        <p class="text-danger">Please Specify a return date.</p>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ redirect()->back()->getTargetUrl() }}" class="btn btn-primary">Back</a>
            </form>
        </div>

    </div>
@endsection