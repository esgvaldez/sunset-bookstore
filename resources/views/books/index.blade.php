@extends('layouts.main')

@section('content')
    <div class="py-5">
        <div class="mb-2">
            <h1 class="m-0">Books</h1> 
            <a href="#">Add stock &#8594;</a>
        </div>
        <div class="mb-4" id="add-book-stock-form">
            <form action="{{ route('books.add') }}" method="post">
                @csrf
                <div class="row mb-3">
                    <div class="col-md-5">
                        <div class="mb-3">
                            <label for="title" class="form-label">Book Title<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="title" name="title">
                            @error('title')
                                <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="author" class="form-label">Book Author<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="author" name="author">
                            @error('author')
                                <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="publisher" class="form-label">Publisher<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="publisher" name="publisher">
                            @error('publisher')
                                <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="form_label">
                            <span class="form_text">Distribute to</span>
                            <div class="multiselect_block">
                                <label for="select-1" class="field_multiselect">Libraries</label>

                                <input id="checkbox-1" class="multiselect_checkbox" type="checkbox">
                                <label for="checkbox-1" class="multiselect_label"></label>

                                <select id="select-1" class="field_select" name="libraries[]" multiple style="@media (min-width: 768px) { height: calc(4 * 38px)}">
                                    @if( $libraries->count() )
                                        @foreach( $libraries as $library )
                                            <option value="{{ $library->id }}">{{ $library->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <span class="field_multiselect_help">You can select several items by pressing <b>Ctrl(or Command)+Element</b></span>
                            </div>
                            @error('libraries')
                                <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-7">
                        <!--  -->
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        @if ( $books->count() )
            @foreach ($books as $book)
                <x-books :book="$book" />
            @endforeach
            {{ $books->links() }}
        @else
            <p>There are no Books</p>
        @endif
    </div>
@endsection