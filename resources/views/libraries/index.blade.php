@extends('layouts.main')

@section('content')
    <div class="py-5">
        <h1>Libraries</h1>
        @if ( $libraries->count() )
            @foreach ($libraries as $library)
                <div class="card mb-3">
                    <div class="card-body">
                        <h5 class="card-title">{{ $library->name }}</h5>
                        <a href="{{ route('library.books', $library) }}" class="btn btn-primary">All Books</a>
                    </div>
                </div>
            @endforeach
            {{ $libraries->links() }}
        @else
            <p>There are no Libraries</p>
        @endif
    </div>
@endsection