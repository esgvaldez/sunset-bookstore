@extends('layouts.main')

@section('content')
    <div class="py-5">
        <h1>Library Books</h1>
        @if ( $books && count($books) )
            @foreach ($books as $book)
                <x-books :book="$book" />
            @endforeach
        @else
            <p>There are no Books in this Library</p>
        @endif
    </div>
@endsection