<div class="card mb-3">
    <div class="card-header">
        <h5 class="card-title">{{ $book->title }}</h5>
    </div>
    <div class="card-body">
        <div class="mb-3">
            <p class="card-text m-0">Author: <strong>{{ $book->author }}</strong></p>
            <p class="card-text">Publisher: <strong>{{ $book->publisher }}</strong></p>
            <p class="card-text">Library: <strong><a href="{{ route('library.books', $book->library) }}">{{ $book->library->name }}</a></strong></p>

            @if( $book->return_date_at !== null)
                <p class="card-text m-0">Availability: <span class="text-danger">OUT</span></p>
                <p class="card-text">Return Date: {{ $book->return_date_at }}</p>
            @else
                <p class="card-text">Availability: <span class="text-success">IN</span></p>
                <a href="{{ route('books.lend', $book) }}" class="btn btn-primary">Lend Book</a>
            @endif

        </div>
    </div>
</div>