<?php

namespace Database\Factories;

use App\Models\Library;
use Illuminate\Database\Eloquent\Factories\Factory;

class LibraryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Library::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $library_name = $this->faker->company();
        $slug = slug_format($library_name);

        return [
            'name' => $library_name,
            'slug' => $slug,
        ];
    }
}
