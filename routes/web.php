<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BooksController;
use App\Http\Controllers\LibrariesController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::get('/login', [LoginController::class, 'index'])->name('login');
Route::post('/login', [LoginController::class, 'authenticateUser']);

Route::post('/logout', [LogoutController::class, 'logoutUser'])->name('logout');

Route::get('/libraries', [LibrariesController::class, 'index'])->name('libraries');
Route::get('/libraries/{library:slug}', [LibrariesController::class, 'showBooks'])->name('library.books');

Route::get('/books', [BooksController::class, 'index'])->name('books');
Route::get('/books/{book}', [BooksController::class, 'lendBookForm'])->name('books.lend');
Route::put('/books/out', [BooksController::class, 'lendBook'])->name('books.out');
Route::post('/books/add', [BooksController::class, 'addToStock'])->name('books.add');
