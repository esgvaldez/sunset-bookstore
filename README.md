## Usage

1. Install NPM and Composer dependencies

- npm install
- composer install

2. Next, run the Migration

- php artisan migrate

3. Seed the database for populating the list of Libraries and as well as create a user

- php db:seed

NOTE: default email and password
- email: user@example.net
- password: password

4. I did not include my own .env file, please don't forget to create one ( copy the example, I know you that ;) )

5. edit .env file and generate your application key

- php artisan key:generate

6. Serve the application on the PHP development server

- php artisan serve

7. Run NPM command 'dev' to compile JS and CSS files to the public folder

- npm run dev

8. Check the app


PS: apologies for the very late submission. But looking forward for a positive feedback and perhaps a good news. Thank you.
