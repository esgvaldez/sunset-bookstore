<?php

namespace App\Models;

use App\Models\Book;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Library extends Model
{
    use HasFactory;

    public function bookStocks(int $library_id) 
    {
        return $this->books()->with(['library']);
    }

    public function books()
    {
        return $this->hasMany(Book::class);
    }
}
