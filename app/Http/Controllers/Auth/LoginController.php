<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{

    public function __construct() 
    {
        $this->middleware(['guest', 'sanitizer']);
    }

    public function index() 
    {
        return view('auth.login');
    }

    public function authenticateUser(Request $request) 
    {
        $credentials = $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ( !auth()->attempt($credentials, $request->remember) ) {
            return back()->with('fail_authentication', 'Invalid Login details');
        }

        return redirect()->route('libraries');
    }
}
