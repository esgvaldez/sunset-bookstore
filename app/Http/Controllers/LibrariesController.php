<?php

namespace App\Http\Controllers;

use App\Models\Library;
use Illuminate\Http\Request;

class LibrariesController extends Controller
{

    public function __construct() 
    {
        $this->middleware(['auth']);
    }

    public function index() 
    {
        $libraries = Library::paginate(10);
        return view('libraries.index', [
            'libraries' => $libraries
        ]);
    }

    public function showBooks(Library $library) 
    {   
        $books = $library->bookStocks($library->id)->paginate(10);
        return view('libraries.books', [
            'books' => $books
        ]);
    }
}
