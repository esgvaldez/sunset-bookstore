<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Library;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class BooksController extends Controller
{

    public function __construct() 
    {
        $this->middleware(['auth', 'sanitizer']);
    }

    public function index() 
    {
        $libraries = Library::all();
        $books = Book::with(['library'])->paginate(10);
        return view('books.index', [
            'libraries' => $libraries,
            'books' => $books
        ]);
    }

    public function addToStock(Request $request)
    {
        $book = $this->validate($request, [
            'title' => 'required',
            'author' => 'required',
            'publisher' => 'required',
            'libraries' => 'required'
        ]);
        
        $distributions = $book['libraries'];
        for ($i = 0; $i < count($distributions); $i++) {
            Book::create([
                'library_id' => $distributions[$i],
                'title' => $book['title'],
                'slug' => slug_format($book['title']),
                'author' => $book['author'],
                'publisher' => $book['publisher']
            ]);
        }

        return back();
    }

    public function lendBookForm(Book $book)
    {
        return view('books.lend', [
            'book' => $book
        ]);
    }

    public function lendBook(Request $request)
    {

        $this->validate($request, [
            'return_date_at' => 'required',
            'return_time_at' => 'required',
        ]);

        $book_id = $request->get('book_id');
        $library_id = $request->get('library_id');
        $return_date = $request->get('return_date_at');
        $return_time = $request->get('return_time_at');
        $return_date_time = date('Y-m-d H:i:s', strtotime("$return_date $return_time"));

        try {

            Book::where([
                'id' => $book_id,
                'library_id' => $library_id,
            ])->update(['return_date_at' => $return_date_time]);


        } catch (\Illuminate\Database\QueryException $e) {
            return back()->with('status', 'Opps, something went wrong: '.$e->getMessage());
        }

        return redirect()->route('libraries');
    }
}
